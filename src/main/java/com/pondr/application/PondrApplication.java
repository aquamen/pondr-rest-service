package com.pondr.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PondrApplication {

    public static void main(String[] args) {
        SpringApplication.run(PondrApplication.class, args);
    }
}
